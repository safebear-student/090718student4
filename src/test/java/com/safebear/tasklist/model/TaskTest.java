package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

/**
 * Created by CCA_Student on 10/07/2018.
 */
public class TaskTest {
    @Test
    public void creation(){

        LocalDate localDate = LocalDate.now();
        Task task = new Task(1l, "Configure Jenkins server", localDate, false);
        Assertions.assertThat(task.getId()).isEqualTo(1L);
//      Now let's check the task name isn't blank
        Assertions.assertThat(task.getName()).isNotBlank();
//      And check the task name.
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins server");
//      Check Date.
        Assertions.assertThat(task.getDueDate()).isEqualTo(localDate);
//      Check it's not yet completed
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);

    }
}
